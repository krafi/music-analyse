import sys
import numpy as np
import pyaudio
import librosa
import librosa.display
import matplotlib.pyplot as plt
from PySide6 import QtWidgets

import threading

class AudioAnalysisApp(QtWidgets.QWidget):
    def __init__(self, song_path):
        super().__init__()
        self.song_path = song_path
        self.fig, self.ax = plt.subplots()
        self.init_ui()

    def init_ui(self):
        self.setGeometry(100, 100, 800, 400)
        self.setWindowTitle('Audio Analysis with Animation')

        self.analysis_label = QtWidgets.QLabel(self)
        self.analysis_label.setGeometry(10, 10, 780, 200)

        self.start_button = QtWidgets.QPushButton('Start Analysis', self)
        self.start_button.setGeometry(10, 220, 120, 30)
        self.start_button.clicked.connect(self.start_analysis)

        self.quit_button = QtWidgets.QPushButton('Quit', self)
        self.quit_button.setGeometry(10, 260, 120, 30)
        self.quit_button.clicked.connect(self.close)

    def start_analysis(self):
        y, sr = librosa.load(self.song_path)
        p = pyaudio.PyAudio()
        stream = p.open(format=pyaudio.paFloat32, channels=1, rate=sr, output=True)

        chunk_size = 2048
        start_idx = 0

        while start_idx < len(y):
            end_idx = min(start_idx + chunk_size, len(y))
            chunk = y[start_idx:end_idx]
            #self.display_analysis(chunk, sr)

            analysis_thread = threading.Thread(target=self.display_analysis(chunk, sr))
            analysis_thread.start()

            stream.write(chunk.tobytes())
            QtWidgets.QApplication.processEvents()
            start_idx = end_idx

        stream.stop_stream()
        stream.close()
        p.terminate()

    def display_analysis(self, chunk, sr):

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.fig.canvas)
        self.analysis_label.setLayout(layout)

        onset_env = librosa.onset.onset_strength(y=chunk, sr=sr)
        chromagram = librosa.feature.chroma_stft(y=chunk, sr=sr)
        mfccs = librosa.feature.mfcc(y=chunk, sr=sr)
        pattern = librosa.feature.tempogram(onset_envelope=onset_env, sr=sr)

        bass = np.mean(chromagram[0])
        midrange = np.mean(chromagram[3:5])
        treble = np.mean(chromagram[6:12])
        timbre = np.mean(mfccs)
        tempo_dynamics = librosa.feature.rhythm.tempo(onset_envelope=onset_env, sr=sr)
        pattern_value = np.mean(pattern)

        '''
        analysis_text = (
            f"Bass: {bass:.2f}\nMidrange: {midrange:.2f}\nTreble: {treble:.2f}\n"
            f"Timbre (MFCCs): {timbre:.2f}\nTempo Dynamics: {tempo_dynamics[0]:.2f}\n"
            f"Pattern: {pattern_value:.2f}"
        )

        self.analysis_label.setText(analysis_text)
        '''

        if not hasattr(self, 'fig'):
            self.fig, self.ax = plt.subplots()
            self.fig.canvas.draw()

        # Update the plot with the calculated data
        self.ax.clear()
        self.ax.barh(['Bass', 'Midrange', 'Treble', 'Timbre', 'Tempo', 'Pattern'],
                    [bass, midrange, treble, timbre, tempo_dynamics[0], pattern_value])
        self.ax.set_title('Audio Analysis')
        self.ax.set_xlabel('Values')
        self.ax.set_ylabel('Features')
        self.fig.canvas.draw()

def main():
    app = QtWidgets.QApplication(sys.argv)
    song_path = 'your_song.mp3'  # Replace with the path to your audio file
    ex = AudioAnalysisApp(song_path)
    ex.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
