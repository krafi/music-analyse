import pyaudio
import numpy as np
import librosa
import librosa.display
import matplotlib.pyplot as plt
import time

def analyze_audio(y, sr):
    onset_env = librosa.onset.onset_strength(y=y, sr=sr)
    tempogram = librosa.feature.tempogram(onset_envelope=onset_env, sr=sr)
    chromagram = librosa.feature.chroma_stft(y=y, sr=sr)

    Bass = np.mean(chromagram[0])
    Midrange = np.mean(chromagram[3:5])
    Treble = np.mean(chromagram[6:12])
    Tempo_Dynamics = librosa.feature.rhythm.tempo(onset_envelope=onset_env, sr=sr)
    Timbre_mfccs = np.mean(librosa.feature.mfcc(y=y, sr=sr))
    pattern = np.mean(librosa.feature.tempogram(onset_envelope=onset_env, sr=sr))
    print("Bass:", Bass)
    print("Midrange:", Midrange)
    print("Treble:", Treble )
    print("Tempo Dynamics:", Tempo_Dynamics )
    print("Timbre (MFCCs):", Timbre_mfccs)
    print("Pattern:", (pattern))
def play_song_with_analysis(song_path):
    y, sr = librosa.load(song_path)
    p = pyaudio.PyAudio()
    stream = p.open(format=pyaudio.paFloat32, channels=1, rate=sr, output=True)

    chunk_size = 2048  # (e.g., 256, 512, 1024, 2048, 4096, 8192)
    start_idx = 0

    analysis_interval = 1
    next_analysis_time = time.time() + analysis_interval

    while start_idx < len(y):
        end_idx = min(start_idx + chunk_size, len(y))
        stream.write(y[start_idx:end_idx].tobytes())

        if time.time() >= next_analysis_time:
            analyze_audio(y[start_idx:end_idx], sr)
            next_analysis_time = time.time() + analysis_interval

        start_idx = end_idx

    stream.stop_stream()
    stream.close()
    p.terminate()
song_path = 'your_song.mp3'
play_song_with_analysis(song_path)
