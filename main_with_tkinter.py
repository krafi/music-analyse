import pyaudio
import numpy as np
import librosa
import librosa.display
import matplotlib.pyplot as plt
import time
import tkinter as tk
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import threading

def view_Canvas(Bass, Midrange, Treble, Tempo_Dynamics, Timbre_mfccs, pattern):
    data_values = [Bass, Midrange, Treble, Tempo_Dynamics, Timbre_mfccs , pattern]

    #data_values_eq_experiment = [Bass, Midrange, Treble, Tempo_Dynamics / 200, Timbre_mfccs / 8, pattern]
    #data_values_10x = [value * 10 for value in data_values_eq_experiment]
    #data_values = data_values_10x

    data_values = np.array(data_values)
    chart.clear()
    chart.bar(data_labels, data_values)
    canvas.draw()
def analyze_audio(y, sr):
    onset_env = librosa.onset.onset_strength(y=y, sr=sr)
    tempogram = librosa.feature.tempogram(onset_envelope=onset_env, sr=sr)
    chromagram = librosa.feature.chroma_stft(y=y, sr=sr)

    Bass = np.mean(chromagram[0])
    Midrange = np.mean(chromagram[3:5])
    Treble = np.mean(chromagram[6:12])
    Tempo_Dynamics = librosa.feature.rhythm.tempo(onset_envelope=onset_env, sr=sr)
    tempo_float = float(Tempo_Dynamics[0])
    Timbre_mfccs = np.mean(librosa.feature.mfcc(y=y, sr=sr))
    pattern = np.mean(librosa.feature.tempogram(onset_envelope=onset_env, sr=sr))
    print("Bass:", Bass)
    print("Midrange:", Bass)
    print("Treble:", Treble )
    print("Tempo Dynamics:", tempo_float )
    print("Timbre (MFCCs):", Timbre_mfccs)
    print("Pattern:", (pattern))
    view_Canvas(Bass, Midrange, Treble, tempo_float, Timbre_mfccs, pattern)

def play_song_with_analysis(song_path):
    y, sr = librosa.load(song_path)
    p = pyaudio.PyAudio()
    stream = p.open(format=pyaudio.paFloat32, channels=1, rate=sr, output=True)

    chunk_size = 2048 # (e.g., 256, 512, 1024, 2048, 4096, 8192)
    start_idx = 0
    analysis_interval = 0.1
    next_analysis_time = time.time() + analysis_interval

    while start_idx < len(y):
        end_idx = min(start_idx + chunk_size, len(y))
        stream.write(y[start_idx:end_idx].tobytes())
        start_idx = end_idx

        if time.time() >= next_analysis_time:
            analyze_audio(y[start_idx - chunk_size:end_idx], sr)
            next_analysis_time = time.time() + analysis_interval

    stream.stop_stream()
    stream.close()
    p.terminate()

# Rest of your code remains the same

# Create the initial Tkinter window
root = tk.Tk()
root.title("Audio Analysis")
fig = Figure(figsize=(12, 8), dpi=100)
chart = fig.add_subplot(111)
data_labels = ["Bass", "Midrange", "Treble", "Tempo Dynamics", "Timbre (MFCCs)", "Pattern"]
data_values = [0, 0, 0, 0, 0, 0]
chart.bar(data_labels, data_values)
canvas = FigureCanvasTkAgg(fig, master=root)
canvas.get_tk_widget().pack()
song_path = 'your_song.mp3'
thread = threading.Thread(target=lambda: play_song_with_analysis(song_path))
thread.start()
root.mainloop()
