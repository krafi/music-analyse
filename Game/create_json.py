import pyaudio
import numpy as np
import librosa
import time
import json
def analyze_audio(y, sr):
    onset_env = librosa.onset.onset_strength(y=y, sr=sr)
    tempogram = librosa.feature.tempogram(onset_envelope=onset_env, sr=sr)
    chromagram = librosa.feature.chroma_stft(y=y, sr=sr)

    Bass = np.mean(chromagram[0])
    Midrange = np.mean(chromagram[3:5])
    Treble = np.mean(chromagram[6:12])
    Tempo_Dynamics = float(librosa.feature.rhythm.tempo(onset_envelope=onset_env, sr=sr))
    Timbre_mfccs = float(np.mean(librosa.feature.mfcc(y=y, sr=sr)))
    pattern = float(np.mean(librosa.feature.tempogram(onset_envelope=onset_env, sr=sr)))

    return {
        "Bass": Bass,
        "Midrange": Midrange,
        "Treble": Treble,
        "Tempo_Dynamics": Tempo_Dynamics,
        "Timbre (MFCCs)": Timbre_mfccs,
        "Pattern": pattern
    }

def analyze_and_store(y, sr):
    analysis_result = []

    chunk_size = 2048
    start_idx = 0
    duration = librosa.get_duration(y=y, sr=sr)
    analysis_interval = 1
    next_analysis_time = analysis_interval

    while start_idx < len(y):
        end_idx = min(start_idx + chunk_size, len(y))
        analysis_time = librosa.samples_to_time(end_idx, sr=sr)
        analysis_data = analyze_audio(y[start_idx:end_idx], sr)
        analysis_result.append({"Time": analysis_time, "Analysis": analysis_data})

        start_idx = end_idx

    return analysis_result

def store_analysis_result_as_json(song_path):
    y, sr = librosa.load(song_path)

    analysis_data = analyze_and_store(y, sr)

    # Convert float32 data to Python float for JSON serialization
    for entry in analysis_data:
        entry["Analysis"]["Tempo_Dynamics"] = float(entry["Analysis"]["Tempo_Dynamics"])
        entry["Analysis"]["Timbre (MFCCs)"] = float(entry["Analysis"]["Timbre (MFCCs)"])
        entry["Analysis"]["Pattern"] = float(entry["Analysis"]["Pattern"])

    with open('audio_analysis.json', 'w') as file:
        json.dump(analysis_data, file, indent=4, default=lambda x: str(x))


song_path = 'a.mp3'
store_analysis_result_as_json(song_path)
