import pyaudio
import numpy as np
import librosa
import json
import time

with open('audio_analysis.json', 'r') as file:
    analysis_data = json.load(file)
song_path = 'a.mp3'
y, sr = librosa.load(song_path)
p = pyaudio.PyAudio()
speed = 1
stream = p.open(format=pyaudio.paFloat32, channels=1, rate=sr * speed, output=True)

for entry in analysis_data:
    analysis_time = entry["Time"]
    analysis_result = entry["Analysis"]
    print(f"Time: {analysis_time} seconds")
    print(f"Analysis: {analysis_result}")
    start_idx = librosa.time_to_samples(analysis_time, sr=sr)
    end_idx = librosa.time_to_samples(analysis_time + 1, sr=sr)
    stream.write(y[start_idx:end_idx])

stream.stop_stream()
stream.close()
p.terminate()
